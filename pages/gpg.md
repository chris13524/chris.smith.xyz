---
title: Chris' GPG key
---

# [Chris](..)' GPG key

Fingerprint: <code>2784A49A280C734C21545A5907C23EF12169858D</code>

Entire public key available <a href="/assets/gpg.txt" resource>here</a>.

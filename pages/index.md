---
title: Chris Smith
---

![Chris' avatar](/assets/chris-smith-lisbon.jpeg)

# Chris Smith

Hi, I'm Chris. I'm a full-stack Rust engineer at Reown.

<dl>
  <dt>Twitter</dt>
  <dd><a href="https://twitter.com/mechris13524">mechris13524</a></dd>

  <div></div>
  <dt>email</dt>
  <dd><a href="mailto:chris@smith.xyz">chris@smith.xyz</a></dd>
  <dt>LinkedIn</dt>
  <dd><a href="https://www.linkedin.com/in/chris13524/">chris13524</a></dd>

  <div></div>
  <dt>GitHub</dt>
  <dd><a href="https://github.com/chris13524">chris13524</a></dd>
  <dt>GitLab</dt>
  <dd><a href="https://gitlab.com/chris13524">chris13524</a></dd>

  <div></div>
  <dt>SSH</dt>
  <dd><a href="/ssh">key</a></dd>
  <dt>GPG</dt>
  <dd><a href="/gpg">key</a></dd>

  <!-- <div></div>
  <dt>services</dt>
  <dd><a href="/dev">software engineer</a></dd> -->
</dl>

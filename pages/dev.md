---
title: "Chris Smith: software engineer"
---

![Chris Smith](/assets/chris-smith.jpg)

# [Chris Smith](/)

<p class="softwareEngineer">software engineer</p>

contact: [chris@smith.xyz](mailto:chris@smith.xyz)  
download: <a resource href="/assets/Christopher Smith Resume.pdf">resume</a>

I am an experienced software engineer who has built a wide range of projects
from [new DNS server](https://gitlab.com/chris13524/tacodns) to
[online giving for non-profits](https://givingtools.com).
